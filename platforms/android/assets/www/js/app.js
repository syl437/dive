// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js


//http://api.openweathermap.org/data/2.5/weather?q=haifa&appid=b4bb2a074add14364340d603a7c2ec92
angular.module('starter', ['ionic', 'starter.controllers','ngStorage','ngCordova', 'pascalprecht.translate', 'ui.select'])

.run(function($ionicPlatform,$rootScope,$ionicPopup,$ionicHistory,$http,$localStorage,$ionicModal,$translate) {
	$rootScope.Host = 'http://ilovedive.co.il/php/';
	$rootScope.DivesCount = 1;
	$rootScope.currentPage = 'main';
	$rootScope.licenseJson = '';
	$rootScope.sendClub = '';
	$rootScope.ClubsDataArray = [];
	$rootScope.LanguagesJson = [];
	$rootScope.EnglishCountries = [];
	$rootScope.HebrewCountries = [];
	$rootScope.AllEnglishCountries = [];
	$rootScope.AllHebrewCountries = [];
	$rootScope.CountryClubsArray = [];
	$rootScope.AboutTextArray = [];
	$rootScope.WeatherLoaded = 0;
	$rootScope.TotalMessagesCount = 0;
	$rootScope.defaultGearsTab = 1;
	$rootScope.WeatherData = [];
	$rootScope.UserGearArray = [];
	$rootScope.WeatherArray = [];
	$rootScope.RandomBannerImage = '';
	$rootScope.SelectedWeatherCity = '';
	$rootScope.SelectedWeatherCityEn = '';
	$rootScope.defaultWeatherCountry = 'IL';
	//$rootScope.DefaultLanguage = 'he';
	$rootScope.UserCountryCode = '';
	
	if (!$localStorage.defaultLanguage)
		$localStorage.defaultLanguage = 'he';

	$rootScope.DefaultLanguage = $localStorage.defaultLanguage;


	$ionicPlatform.registerBackButtonAction(
	function () 
	{
	 if( $rootScope.currentPage == "main")
	 {
		var confirmPopup = $ionicPopup.confirm({
			 title: 'האם לצאת מהאפליקצייה?',
		   });
		   confirmPopup.then(function(res) {
			 if(res) {
			   navigator.app.exitApp();
			 } else {
			   event.preventDefault();
			   //window.history.back();
			   //$ionicHistory.goBack(1);
			 }
		   });			 
	 }
	 else
	 {
		window.history.back();
	 }
	}, 100
    );
	

  
  $ionicPlatform.ready(function() {


				
	$http.get('js/data/languages.json').success(function(data)
	{
		$rootScope.LanguagesJson = data;
	});



	//countries - en
	$http.get('js/data/countries_en.json').success(function(data)
	{
		$rootScope.EnglishCountries = data;
	});	
	
	//countries - he
	$http.get('js/data/countries_he.json').success(function(data)
	{
		$rootScope.HebrewCountries = data;
	});	
	
	
	//all countries - en
	$http.get('js/data/all_countries_en.json').success(function(data)
	{
		$rootScope.AllEnglishCountries = data;
	});	
	
	//all countries - he
	$http.get('js/data/all_countries_he.json').success(function(data)
	{
		$rootScope.AllHebrewCountries = data;
	});	
	

	//get about text
	$http.get($rootScope.Host+'/get_about_info.php').success(function(data)
	{
		$rootScope.AboutTextArray = data;
		console.log("about/terms:" , data);
	});	

	
	//get clubs by countries
	$http.get($rootScope.Host+'/get_clubs_by_country.php').success(function(data)
	{
		$rootScope.CountryClubsArray = data;
	});	
	
	//get homepage banners
	//get_clubs_banners.php
	$http.get($rootScope.Host+'/get_homepage_banners.php').success(function(data)
	{
		$rootScope.ClubsBannersArray = data;
		console.log("ClubsBannersArray",$rootScope.ClubsBannersArray);
		$rootScope.RandomBanner = Math.floor(Math.random() * $rootScope.ClubsBannersArray.length);
		$rootScope.RandomBannerImage = $rootScope.ClubsBannersArray[$rootScope.RandomBanner].image;
		
	});		
	
	
	// get if gear requires annual check

      $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
      $http.post($rootScope.Host+'/check_gear.php',{user: $localStorage.userid}).then(function(data){
      	for (var i = 0; i < data.data.length; i++){
            $ionicPopup.alert({
                title:  $translate.instant("check1") + "<br>"+$translate.instant(data.data[i].date) + "<br>"+ $translate.instant("check2") + $translate.instant(data.data[i].type) + "<br>"+ $translate.instant("check3"),
                buttons: [{
                    text: $translate.instant('clubsmain_close'),
                    type: 'button-positive',
                }]
            });
		}
	  })

	
	//get weather
	//js/data/weather_countries.json
	$http.get($rootScope.Host+'/get_weather.php').success(function(data)
	{
		$rootScope.WeatherCountries = data;
		$rootScope.WeatherIndex = '';
		console.log("get_weather",data);
		for (i = 0; i < $rootScope.WeatherCountries.length; i++) 
		{
			if ($rootScope.UserCountryCode)
			{
				if ($rootScope.UserCountryCode == $rootScope.WeatherCountries[i].country_code)
				{
					$rootScope.WeatherArray.push($rootScope.WeatherCountries[i]);
				}				
			}

			else
			{
				if ($rootScope.WeatherCountries[i].country_code == $rootScope.defaultWeatherCountry)
				{
					$rootScope.WeatherArray.push($rootScope.WeatherCountries[i]);
				}				
			}

		}

				
		console.log("$rootScope.WeatherArray",$rootScope.WeatherArray);
		
		if ($rootScope.WeatherArray.length > 0)
		{
			for (i = 0; i < $rootScope.WeatherArray.length; i++) 
			{
				$rootScope.WeatherArray[i].selected = "0";
			}
			$rootScope.WeatherArray[0].selected = "1";
			$rootScope.GetWeatherAPI($rootScope.WeatherArray[0].location_lat,$rootScope.WeatherArray[0].location_lng);
			$rootScope.SelectedWeatherCity = $rootScope.WeatherArray[0].title;
			$rootScope.SelectedWeatherCityEn = $rootScope.WeatherArray[0].title_english;
		}
	});


	$rootScope.GetWeatherAPI = function(lat,lng)
	{
		$http.get('http://api.openweathermap.org/data/2.5/weather?lat='+lat+'&lon='+lng+'&appid=d954ae2732549b677fc73fcd1031b440&units=metric').success(function(data)
		{
			$rootScope.WeatherLoaded = 1;
			$rootScope.WeatherData = data;
			//console.log ("weather: " , data);
		});		
	}

	//$rootScope.GetWeatherAPI(32.083328,34.799999);
	
	

/* 
	if(typeof navigator.globalization !== "undefined") {
		navigator.globalization.getPreferredLanguage(function(language) {
			$translate.use((language.value).split("-")[0]).then(function(data) {
				console.log("SUCCESS -> " + data);
			}, function(error) {
				console.log("ERROR -> " + error);
			});
		}, null);
	}
*/


	function successCallback(result) {
		$rootScope.UserCountryCode =  result.countryCode.toUpperCase();
		//alert ($rootScope.UserCountryCode)
	  //alert(JSON.stringify(result));
	}

	function errorCallback(error) {
	  //alert(JSON.stringify(error));
	}

	
	
	window.plugins.sim.getSimInfo(successCallback, errorCallback);
	
	
	
	
	document.addEventListener("deviceready", function () {
		window.ga.startTrackerWithId('UA-102697537-1');
		 cordova.plugins.notification.badge.clear();
		 cordova.plugins.notification.badge.configure({ autoClear: true });

	})

	
  var notificationOpenedCallback = function(jsonData) {
	 
	 if (jsonData.isAppInFocus)
	 {
		if (jsonData.payload.body)
		{
			$ionicPopup.alert({
			 title: jsonData.payload.body,
			buttons: [{
				text: $translate.instant('confirm_error'),
				type: 'button-positive',
			  }]
		   });

			$rootScope.$broadcast('pushmessage',jsonData);	
		}
	 }
	
  };
  

  

  if (window.cordova)
  {
	  
	
	document.addEventListener('deviceready', function() {

		window.plugins.OneSignal
			.startInit("90703417-5111-4ad7-929e-bd06537fdb3a")
			.inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.None)			
			.handleNotificationReceived(notificationOpenedCallback)
			.handleNotificationOpened(notificationOpenedCallback)
			.endInit();

		window.plugins.OneSignal.getIds(function (ids) {

			$rootScope.pushId = ids.userId;
			
			if (ids.userId)
			{
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
				
				send_data = 
				{
					"user" : $localStorage.userid,
					"push_id" : $rootScope.pushId
				}					
				$http.post($rootScope.Host+'/update_push.php',send_data).success(function(data)
				{
					
				});					
			}
		});
		
		//OneSignal.clearOneSignalNotifications();
		//window.plugins.OneSignal.clearOneSignalNotifications();
						 
	  // Show an alert box if a notification comes in when the user is in your app.
	  //window.plugins.OneSignal.enableInAppAlertNotification(false);
	  window.plugins.OneSignal.enableNotificationsWhenActive(false);
	  
	  
	
	});  
	

	
	  
  }   


	
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider, $translateProvider) {
	
  // configures staticFilesLoader
  $translateProvider.useStaticFilesLoader({
    prefix: 'js/data/locale-',
    suffix: '.json'
  });
  // load 'en' table on startup
  $translateProvider.preferredLanguage('he');
  $translateProvider.fallbackLanguage('he');

	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.main', {
    url: '/main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
		controller: 'MainCtrl'
      }
    }
  })
  
  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
		controller: 'LoginCtrl'
      }
    }
  })

  .state('app.register', {
    url: '/register',
    views: {
      'menuContent': {
        templateUrl: 'templates/register.html',
		controller: 'RegisterCtrl'
      }
    }
  })

  .state('app.divinglicense', {
    url: '/divinglicense',
    views: {
      'menuContent': {
        templateUrl: 'templates/diving_license.html',
		controller: 'DivingLicenseCtrl'
      }
    }
  })  

  .state('app.divingdiary', {
    url: '/divingdiary',
    views: {
      'menuContent': {
        templateUrl: 'templates/diving_diary.html',
		controller: 'DivingDiaryCtrl'
      }
    }
  })  
  
  .state('app.addlisense', {
    url: '/addlisense/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/add_lisense.html',
		controller: 'AddLicenseCtrl'
      }
    }
  })  
  
  .state('app.adddiary', {
    url: '/adddiary/:ItemId/:Count',
    views: {
      'menuContent': {
        templateUrl: 'templates/add_diary.html',
		controller: 'AddDiaryCtrl'
      }
    }
  })    
 
  .state('app.clubs', {
    url: '/clubs',
    views: {
      'menuContent': {
        templateUrl: 'templates/clubs.html',
		controller: 'ClubsCtrl'
      }
    }
  })  
  
  .state('app.clubs_details', {
    url: '/clubs_details/:IndexId/:ClubId',
    views: {
      'menuContent': {
        templateUrl: 'templates/clubs_details.html',
		controller: 'ClubsDetailsCtrl'
      }
    }
  })  
  

    .state('app.insurance', {
    url: '/insurance',
    views: {
      'menuContent': {
        templateUrl: 'templates/insurance.html',
		controller: 'InsuranceCtrl'
      }
    }
  })  

    .state('app.gearscheck', {
    url: '/gearscheck',
    views: {
      'menuContent': {
        templateUrl: 'templates/gearscheck.html',
		controller: 'GearsCheckMainCtrl'
      }
    }
  }) 

    .state('app.gears_primary', {
    url: '/gears_primary',
    views: {
      'menuContent': {
        templateUrl: 'templates/gears_primary.html',
		controller: 'GearsPrimaryCtrl'
      }
    }
  }) 

    .state('app.manage_gears', {
    url: '/manage_gears/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/manage_gears.html',
		controller: 'ManageGearsCtrl'
      }
    }
  }) 

  
    .state('app.contact', {
    url: '/contact',
    views: {
      'menuContent': {
        templateUrl: 'templates/contact.html',
		controller: 'ContactCtrl'
      }
    }
  })  
    .state('app.updateinfo', {
    url: '/updateinfo',
    views: {
      'menuContent': {
        templateUrl: 'templates/updateinfo.html',
		controller: 'UpdateInfoCtrl'
      }
    }
  })  

    .state('app.mainlicense', {
    url: '/mainlicense',
    views: {
      'menuContent': {
        templateUrl: 'templates/mainlicense.html',
		controller: 'MainLicenseCtrl'
      }
    }
  })  

    .state('app.coupons', {
    url: '/coupons',
    views: {
      'menuContent': {
        templateUrl: 'templates/coupons.html',
		controller: 'CouponsCtrl'
      }
    }
  })  

    .state('app.manage_coupon', {
    url: '/manage_coupon/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/manage_coupon.html',
		controller: 'ManageCouponCtrl'
      }
    }
  })    
  
    .state('app.messages', {
    url: '/messages',
    views: {
      'menuContent': {
        templateUrl: 'templates/messages.html',
		controller: 'MessagesCtrl'
      }
    }
  })    
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
